var swiper = new Swiper('.swiper-container', {
   effect: 'coverflow',
   grabCursor: true,
   centeredSlides: true,
   slidesPerView: 'auto',
   coverflowEffect: {
     rotate: 50,
     stretch: 0,
     depth: 100,
     modifier: 1,
     slideShadows : false,
   },
   pagination: {
     el: '.swiper-pagination',
   },
 });


 AOS.init({
   duration:1200,
 });

//  window.addEventListener("hashchange", function () {
//     window.scrollTo(window.scrollX, window.scrollY - 50);
// });

var smothScroll =new scrolltoanchor({
  duration : 2000,
  offset : 50
});
